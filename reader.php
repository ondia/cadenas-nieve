<?php
/**
 * Created by PhpStorm.
 * User: starm
 * Date: 2/6/2018
 * Time: 16:56
 */


include('vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php');


$nameInputFile = "bbdd_cadenas_2016.xls";
$allModelsStr = "GripTEX - GT0,GripTEX - GT1,GripTEX - GT2,GripTEX - GT3,GripTEX - GT4,GripTEX - GT5,GripTEX - GT6,GripTEX - GT7,".
    "TRAK 205,TRAK 206,TRAK 207,TRAK 208,TRAK 209,TRAK 210,TRAK 211,TRAK 212,TRAK 213,TRAK 214,TRAK 215,TRAK 216,TRAK 217,TRAK LT44," .
    "TRAK LT45,TRAK LT46,TRAK LT47,TRAK LT48,TRAK LT49,TRAK LT50,TRAK LT51,TRAK LT52,TRAK LT53,TRAK LT54,TRAK LT55,ULTRA 20,ULTRA 30,".
    "ULTRA 40,ULTRA 50,ULTRA 60,ULTRA 70,ULTRA 80,ULTRA 90,ULTRA 100,ULTRA 110,ULTRA 120,ULTRA 360,ULTRA 370,ULTRA 380,ULTRA 390,ULTRA 400,".
    "ULTRA 410,ULTRA 450,ULTRA 460,ULTRA 470,ULTRA 480,EASY A11,EASY B11,EASY C11,EASY C12,EASY D11,EASY E11,EASY G12,EASY G13,EASY G14,EASY H12,".
    "EASY H13,EASY H14,EASY J11,EASY K14,EASY K15,EASY L13,EASY L12,EASY M13,EASY R12,EASY S11,EASY S12,EASY S14,EASY T11,EASY T12,EASY W12,".
    "EASY T13,EASY U11,EASY X12,EASY X13,EASY Y11,EASY Z11,EASY T14,EASY W13,EASY X14,EASY K16,EASY L14,EASY M14,EASY M15,EASY T15,EASY J12,".
    "COMPACT 1,COMPACT 2,COMPACT 3,COMPACT 4,ALPINE 1,ALPINE 2,ALPINE_PRO 1,ALPINE_PRO 2,ALPINE_PRO 3,ALPINE_PRO 4,ALPINE_PRO 5,PLUS 20,PLUS 30,".
    "PLUS 40,PLUS 50,PLUS 60,PLUS 65,PLUS 70,PLUS 80,PLUS 85,PLUS 95,PLUS 102,PLUS 104,PLUS 100,FIX_XS,FIX_S,FIX_A,FIX_B,FIX_C,FIX_D,FIX_E,".
    "FIX_F,FIX_G,FIX_H,FIX_I,FIX_J,FIX_K,FIX_L1,FIX_M1,FIX_N1,FIX_O1,FIX_P1,FIX_Q1,FIX_R1,FIX_S1,WALK M,WALK L,WALK XL,WALK XXL,Show 7 - T.S11,".
    "Show 7 - T.S12,Show 7 - T.S10,Show 7 - T.S13,Show 7 - T.S14,Show 7 - T.S52,Show 7 - T.S53,Show 7 - T.S51,Show 7 - T.S54,Show 7 - T.S81,".
    "Show 7 - T.S82,Show 7 - T.S83,Show 7 - T.S84,Show 7 - T.S86,Show 7 - T.S85,Show 7 - T.S87,Show 7 - T.S89,Show 7 - T.S88,Show 7 - T.S110,".
    "Show 7 - T.S120,Show 7 - T.S130,X-10 - T.310,X-10 - T.320,X-10 - T.330,X-10 - T.340,X-10 - T.315,X-10 - T.350,X-10 - T.325,X-10 - T.335,".
    "X-10 - T.345,X-10 - T.355,X-10 - T.360,X-10 - T.370,X-10 - T.357,X-10 - T.380,X-10 - T.365,X-10 - T.375,CG9 - T.20,CG9 - T.30,CG9 - T.40,CG9 - T.50,".
    "CG9 - T.60,CG9 - T.35,CG9 - T.70,CG9 - T.80,CG9 - T.45,CG9 - T.65,CG9 - T.90,CG9 - T.100,CG9 - T.95,CG9 - T.55,CG9 - T.75,CG9 - T.104,CG9 - T.97,".
    "CG9 - T.102,CG9 - T.107,CG9 - T.105,CG9 - T.103,CG9 - T.117,CG9 - T.106,XG12 - T. 190,XG12 - T. 200,XG12 - T. 210,XG12 - T. 220,XG12 - T. 225,XG12 - T. 230,".
    "XG12 - T. 240,XG12 - T. 245,XG12 - T. 247,XG12 - T. 255,XG12 - T. 250,XG12 - T. 235,XG12 - T. 265,XG12 - T. 267,SOCK - T. 540,SOCK - T. 600,SOCK - T. 645,".
    "SOCK - T. 685,SOCK - T. 695,SOCK - T. 697,SOCK - T. 698,SOCK - T. 699,Easy-fit² - T.050,Easy-fit² - T.055,Easy-fit² - T.060,Easy-fit² - T.065,".
    "Easy-fit² - T.070,Easy-fit² - T.075,Easy-fit² - T.080,Easy-fit² - T.090,Easy-fit² - T.095,Easy-fit² - T.097,Easy-fit² - T.100,Easy-fit² - T.102,Easy-fit² - T.103,".
    "Easy-fit² - T.104,EASY Y3,EASY A1,EASY A2,EASY A3,EASY J1,EASY J2,EASY J3,EASY J4,EASY J6,EASY J7,EASY B1,EASY B2,EASY B3,EASY C2,EASY C3,EASY C4,EASY C5,EASY D1,EASY D4,EASY D5,EASY D6,EASY Q1,EASY Q2,EASY Q3,EASY Q4,EASY Q5,EASY Q6,EASY Q7,EASY CC1,EASY CC2,EASY CC3,EASY CC4,EASY K1,EASY K2,EASY K3";

try {

    // Encoding properly, the given string with the chains model names
    $allModelsEncoded = iconv(mb_detect_encoding($allModelsStr, mb_detect_order(), true), "UTF-8", $allModelsStr);
    $allModels = explode(",", $allModelsEncoded);

    // Getting the Object Excel
    $inputFileType = PHPExcel_IOFactory::identify($nameInputFile);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objExcel = $objReader->load($nameInputFile);


    $sheet = $objExcel->getSheet();
    $max_row = $sheet->getHighestRow();
    $max_col = $sheet->getHighestColumn();


    foreach($allModels as $key => $model) {
        findTitle($sheet, $model);
        //echo "<br/> He acabat de buscar: ($model)" . $key . "<br/>";
    }

    echo "Final del programa";


} catch (Exception $e) {
    echo $e->getMessage();
    die();
}





/**
 * @param $sheet
 * @param $findTitle
 */
function findTitle($sheet, $findTitle) {

    $max_row = $sheet->getHighestRow();
    //$letters_str = 'E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X';
    $letters_str = 'N,R,S,G,I,L,M,U,V,W,H,P,O,T,F,J,E,K'; // ordered as referencias_tab
    $letters = explode(',', $letters_str);
    $allSizes = "";
    $saved = array();

    try {

        //for ($col = 0; $col < count($letters); $col++) {

            for ($row = 0; $row <= $max_row; $row++) {

                $val = $sheet->getCell($letters[4] . $row)->getFormattedValue();

                if($findTitle == $val) {

                    array_push($saved, $row);
                    //echo "Ok. " . $letters[$col] . "<br/>";

                } else {
                    //echo "not found: " . $val . " | " . $findTitle . "<br/>";
                }
            }
        //}

        foreach($saved as $value) {

            $allSizes .= $sheet->getCell('D'. $value) . ", ";
        }

        if (count($saved) > 0) {
            echo $findTitle . ": " . $allSizes . "<br/>";
        }

    } catch(Exception $e) {
        echo $e->getMessage();
    }
}